import { Component, OnInit } from '@angular/core';
import { TaskService } from '../task.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit {

  private tasks = [];

  constructor(private service: TaskService,
    private router: Router,
    private aroute: ActivatedRoute) { }

  ngOnInit() {
    this.service.getTasks().subscribe(data => {
      this.tasks = data;
      this.tasks.sort(function(a, b) {
        return a.id - b.id;
      });
    });
  }

  details(id: number) {
    this.router.navigate(['/taskDetail', id]);
  }

  updateTask(id: number) {
    this.router.navigate(['/editTask', id]);
  }

  delete(id: number, i: number) {
    this.service.deleteTask(id).subscribe();
    this.tasks.splice(i, 1);
  }

}
