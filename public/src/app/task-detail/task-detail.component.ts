import { Component, OnInit } from '@angular/core';
import { Task } from '../task';
import { TaskService } from '../task.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-task-detail',
  templateUrl: './task-detail.component.html',
  styleUrls: ['./task-detail.component.css']
})
export class TaskDetailComponent implements OnInit {

  public task: Task;

  constructor(private tasks: TaskService,
    private router: Router,
    private aroute: ActivatedRoute) { }

  ngOnInit() {
    this.aroute.paramMap.subscribe((param: ParamMap) => {
      const i = parseInt(param.get('id'), 0);
      this.tasks.getTaskById(i)
      .subscribe(data => this.task = data);
    });
  }

}
