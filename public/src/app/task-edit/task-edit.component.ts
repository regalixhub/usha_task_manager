import { Component, OnInit } from '@angular/core';
import { TaskService } from '../task.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Task } from '../task';

@Component({
  selector: 'app-task-edit',
  templateUrl: './task-edit.component.html',
  styleUrls: ['./task-edit.component.css']
})
export class TaskEditComponent implements OnInit {

  public id: number;
  public task: Task;

  constructor(private tasks: TaskService,
    private router: Router,
    private aroute: ActivatedRoute) { }

  ngOnInit() {
    this.aroute.paramMap.subscribe((param: ParamMap) => {
      this.id = parseInt(param.get('id'), 10);
      this.tasks.getTaskById(this.id).subscribe(data => this.task = data);
      // dependency injection
    });
  }

  saveTask(data) {
    this.tasks.updateTasks(data).subscribe(() =>
    this.router.navigate(['/listTask'])
    );
  }


}
