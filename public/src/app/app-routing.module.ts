import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TaskListComponent } from './task-list/task-list.component';
import { TaskAddComponent } from './task-add/task-add.component';
import { TaskEditComponent } from './task-edit/task-edit.component';
import { TaskDetailComponent } from './task-detail/task-detail.component';

const routes: Routes = [
  {path: '', redirectTo: 'listTask', pathMatch: 'full'},
  {path: 'addTask', component: TaskAddComponent},
  {path: 'listTask', component: TaskListComponent},
  {path: 'taskDetail/:id', component: TaskDetailComponent},
  {path: 'editTask/:id', component: TaskEditComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
